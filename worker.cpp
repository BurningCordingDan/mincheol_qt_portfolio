#include "worker.h"

#include <QMutex>
#include <QWaitCondition>

//!
//!     공유자원에 대해 접근하여 생산하고 소비하는 클래스 입니다.
//!     해당 클래스는 Thread 내부에서의 동작을 가정하여 설계되어 있습니다.
//!
//!     원할한 update를 위해 _UPDATE_LIMIT를 이용 signal 발생 횟수를 제한 하고 있습니다.
//!

Worker::Worker(QObject *parent) : QObject(parent),
    m_value(NULL),
    m_waitCondition(NULL),
    m_valueMutex(NULL),
    m_workFlag(false)
{

}

Worker::~Worker()
{

}

void Worker::make()
{
    if(!m_value || !m_valueMutex || !m_waitCondition)
        return;

    while(*m_value != -1)
    {
        QMutexLocker locker(m_valueMutex);
        if(*m_value < _VALUE_MAX_COUNT)
        {
            // [1] 값이 max 이내일 시 값 증가.
            (*m_value) += 1;

            if(*m_value % _UPDATE_LIMIT == 0)
                emit updateRequest();
        }
        else
        {
            // [2] 값이 가득 찻기 때문에 consumer 깨우기.
            m_waitCondition->wakeOne();
            setWorkFlag(false);

            // [3] consumer가 전부 소비할때 까지 대기.
            m_waitCondition->wait(m_valueMutex);
            locker.unlock();
            setWorkFlag(true);
        }
    }
}

void Worker::consume()
{
    if(!m_value || !m_valueMutex || !m_waitCondition)
        return;

    while(*m_value != -1)
    {
        QMutexLocker locker(m_valueMutex);
        if(0 < *m_value )
        {
            // [1] 값이 존재 할 시 해당 값 감소.
            (*m_value) -= 1;

            if(*m_value % _UPDATE_LIMIT == 0)
                emit updateRequest();
        }
        else
        {
            // [2] 값이 없기 때문에 maker 깨우기.
            m_waitCondition->wakeOne();
            setWorkFlag(false);

            // [3] maker가 생산을 종료할때 까지 대기.
            m_waitCondition->wait(m_valueMutex);
            locker.unlock();
            setWorkFlag(true);
        }
    }
}

void Worker::setWorkFlag(volatile bool workFlag)
{
    m_workFlag = workFlag;

    emit workFlagChanged(m_workFlag);
}

void Worker::setValue(volatile int *value)
{
    m_value = value;
}

void Worker::setMutex(QMutex *mutex)
{
    m_valueMutex = mutex;
}

void Worker::setWaitCondition(QWaitCondition *waitCondition)
{
    m_waitCondition = waitCondition;
}
