#ifndef WORKER_H
#define WORKER_H

#include <QObject>

#define _VALUE_MAX_COUNT    100000000
#define _UPDATE_LIMIT       10000

class QWaitCondition;
class QMutex;

class Worker : public QObject
{
    Q_OBJECT
public:
    explicit Worker(QObject *parent = 0);
    ~Worker();

    void setWaitCondition(QWaitCondition *waitCondition);
    void setMutex(QMutex *mutex);
    void setValue(volatile int *value);

    void setWorkFlag(volatile bool workFlag);

signals:
    void workFlagChanged(bool);
    void updateRequest();

public slots:
    void make();
    void consume();


private:
    volatile int    *m_value;
    QWaitCondition  *m_waitCondition;
    QMutex          *m_valueMutex;

    volatile bool   m_workFlag;
};

#endif // WORKER_H
