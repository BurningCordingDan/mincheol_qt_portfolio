#ifndef SORTWIDGET_H
#define SORTWIDGET_H

#include <QWidget>

namespace Ui {
class sortwidget;
}

class SortWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SortWidget(QWidget *parent = 0);
    ~SortWidget();

protected:
    virtual void sortFunction(int limit) = 0;

    void timerEvent(QTimerEvent *event);
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

    void calcMatrix(QMatrix &matrix);
    void makeSquareRect(QRect &rect);

    void shfflePoint(QList<QPoint> &list);

protected:
    Ui::sortwidget *ui;

    int m_timerID;
    QRect m_drawingArea;

    int m_currentIndex;

    QList<QPoint> m_sortedPointList;
    QList<QPoint> m_oldPointList;
};

#endif // SORTWIDGET_H
