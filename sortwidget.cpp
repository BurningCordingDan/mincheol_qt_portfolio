#include "sortwidget.h"
#include "ui_sortwidget.h"

#include <stdio.h>
#include <QPainter>

#define _TIMER_INTERVAL 100
#define _POINT_LIMIT    400

SortWidget::SortWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::sortwidget),
    m_timerID(0),
    m_currentIndex(0)
{
    ui->setupUi(this);

    // [1] timerEvent 시작 및 ID 저장.
    m_timerID = startTimer(_TIMER_INTERVAL);
}

SortWidget::~SortWidget()
{
    killTimer(m_timerID);

    delete ui;
}

void SortWidget::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event)

    if(m_drawingArea.isEmpty())
        return;

    if(m_oldPointList.isEmpty() && m_sortedPointList.isEmpty())
    {

        for(int i = 0; i < _POINT_LIMIT; i++)
        {
            int x = i*2;
            int y = i*2;

            QPoint newPoint(x,y);
            m_oldPointList.append(newPoint);
        }

        shfflePoint(m_oldPointList);
        m_sortedPointList.append(m_oldPointList);
        m_currentIndex = 0;
    }

    if(m_currentIndex == _POINT_LIMIT)
    {
        m_sortedPointList.clear();
        m_oldPointList.clear();
        return;
    }

    sortFunction(_POINT_LIMIT);



    repaint();
    m_currentIndex += 1;
}

void SortWidget::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)

    // [1] 현재의 Rect를 구하여 정사각형으로 변환 및 세팅.
    QRect currentRect = rect();
    makeSquareRect(currentRect);

    m_drawingArea = currentRect;

    m_sortedPointList.clear();
    m_oldPointList.clear();
}

void SortWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    if(m_drawingArea.isEmpty())
        return;


    QPainter painter(this);

    // [1] 배경.
    painter.setPen(Qt::black);
    painter.setBrush(Qt::black);
    painter.drawRect(rect());

    // [2] 화면 중심으로 오도록 Matrix 세팅.
    QMatrix matrix;
    calcMatrix(matrix);
    painter.setMatrix(matrix);

    // oldPen
    QPen grayPen;
    grayPen.setColor(Qt::lightGray);
    grayPen.setWidth(4);

    painter.setPen(grayPen);
    painter.setBrush(Qt::lightGray);

    foreach (const QPoint &oldPoint, m_oldPointList) {
        painter.drawPoint(oldPoint);
    }

    // sortPen
    QPen redPen;
    redPen.setColor(Qt::red);
    redPen.setWidth(4);

    painter.setPen(redPen);
    painter.setBrush(Qt::red);

    foreach (const QPoint &sortPoint, m_sortedPointList) {
        painter.drawPoint(sortPoint);
    }

    // guidePen
    QPen yellowPen;
    yellowPen.setColor(Qt::yellow);
    yellowPen.setWidth(0);

    painter.setPen(yellowPen);
    painter.setBrush(Qt::yellow);

    painter.drawLine(0,0,_POINT_LIMIT *2, _POINT_LIMIT *2);

    painter.end();

}

void SortWidget::calcMatrix(QMatrix &matrix)
{
    double centerX = rect().center().x();
    double centerY = rect().center().y();

    // [1] 화면 중심을 0,0으로 옮기기.
    QMatrix moveZeroMatrix;
    moveZeroMatrix.translate(-centerX, -centerY);

    // [2] scale 적용.
    double scale = 0.9;
    QMatrix scaleMatrix;
    scaleMatrix.scale(scale, scale);

    // [3] [1] 과정 원상복구.
    QMatrix moveCenterMatrix;
    moveCenterMatrix.translate(centerX, centerX);


    // [4] QuadTree가 화면 중심으로 오도록 추가 이동.
    double curWidth = rect().width();
    double curHeight = rect().height();

    double areaWidth = m_drawingArea.width();
    double areaHeight = m_drawingArea.height();

    double moveXGap = (curWidth - areaWidth) / 2.0;
    double moveYGap = (curHeight - areaHeight) / 2.0;

    QMatrix moveMatrix;
    moveXGap *= scale;
    moveYGap *= scale;
    moveMatrix.translate(moveXGap, moveYGap);

    // [5] Matrix 적용.
    matrix *= moveZeroMatrix;
    matrix *= scaleMatrix;
    matrix *= moveCenterMatrix;
    matrix *= moveMatrix;
}

void SortWidget::makeSquareRect(QRect &rect)
{
    int width = rect.width();
    int height = rect.height();
    if(width == height)
        return;

    // [1] 작은 크기로 맞춤.
    if(height < width)  rect.setWidth(height);
    else                rect.setHeight(width);
}

void SortWidget::shfflePoint(QList<QPoint> &list)
{
    QList<QPoint> backup = list;
    list.clear();

    // 순서 섞기.
    while (!backup.isEmpty()) {
        QPoint point = backup.at(rand() % backup.size());

        list.append(point);
        backup.removeOne(point);
    }

    // x값 변조.
    backup.clear();
    backup = list;
    list.clear();
    foreach (QPoint pt, backup) {
        int index = backup.indexOf(pt);

        pt.setX(index * 2);
        list.append(pt);
    }
}

