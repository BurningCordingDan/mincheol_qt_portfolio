#include "startwidget.h"
#include "ui_startwidget.h"

#include <QPropertyAnimation>
#include <QTimer>

//!
//!     초기 화면을 보여주는 Widget입니다.
//!     widget animation 세팅이 되어 있습니다.
//!

StartWidget::StartWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StartWidget),
    m_animation(NULL),
    m_animSquence(_SEQUENCE_IN)
{
    ui->setupUi(this);

    // [1] Label animation 생성.
    m_animation = new QPropertyAnimation(ui->label_animation, "geometry",this);

    // [2] Label message 생성.
    makeAnimMessage();

    // [3] 버튼 클릭 Signal 전달.
    connect(ui->pushButton,SIGNAL(clicked(bool)), this, SIGNAL(buttonClicked()));
}

StartWidget::~StartWidget()
{
    delete ui;
}

void StartWidget::animation()
{
    // [1] 라벨 영역 추출 및 좌상단 점을 부모 상대 좌표로 변환.
    QRect labelRect = ui->label_animation->rect();
    QPoint tl = ui->label_animation->mapToParent(labelRect .topLeft());

    // [2] 라벨에 적힌 String의 길이 추출.
    QFontMetrics fm(ui->label_animation->font());
    QString text = ui->label_animation->text();

    int top = tl.y();
    int left = tl.x();
    int w = fm.width(text);
    int h = labelRect.height();

    // [3] Animation 과정을 4단계로 분할.
    int duration = -1;
    switch (m_animSquence) {
    case  _SEQUENCE_READY:
    {
        // [3-1] 왼쪽 화면 밖에서 label 세팅.
        duration = 4;
        m_animation->setDuration(duration);
        setAnimMessage();

        int start = -w*2;
        int destination = -w*4;
        m_animation->setStartValue(QRect(start, top, w, h));
        m_animation->setEndValue(QRect(destination, top, w, h));

        m_animation->start();
        m_animSquence = _SEQUENCE_IN;
    }
        break;
    case _SEQUENCE_IN:
    {
        // [3-2] 왼쪽 화면 에서 화면 중심으로 이동.
        duration = 1200;
        m_animation->setDuration(duration);

        QRect widgetRect = parentWidget()->rect();
        int start = widgetRect.right();
        int destination = (start/2) - (w/2);

        m_animation->setStartValue(QRect(start, top, w, h));
        m_animation->setEndValue(QRect(destination, top, w, h));

        m_animation->start();
        m_animSquence = _SEQUENCE_STAY;
    }
        break;
    case _SEQUENCE_STAY:
    {
        // [3-3] 대기.
        repaint();

        duration = 3000;
        m_animSquence = _SEQUENCE_OUT;
    }
        break;
    case _SEQUENCE_OUT:
    {
        // [3-4] 화면 중심에서 화면 오른쪽으로 이동.
        duration = 1200;
        m_animation->setDuration(duration);

        int destination = -w*2;

        m_animation->setStartValue(QRect(left, top, w, h));
        m_animation->setEndValue(QRect(destination, top, w, h));

        m_animation->start();
        m_animSquence = _SEQUENCE_READY;
    }
        break;
    default:
        break;
    }

    // [4] 설정된 Duration 값 이후 animation 함수 실행.
    QTimer::singleShot(duration,this, SLOT(animation()));
}

void StartWidget::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    startAnimation();
}

void StartWidget::hideEvent(QHideEvent *event)
{
    QWidget::hideEvent(event);
    endAnimation();
}

void StartWidget::startAnimation()
{
    animation();
}

void StartWidget::endAnimation()
{
    m_animation->stop();
}

void StartWidget::makeAnimMessage()
{
    m_messageList << "Made by Kim MinCheol.";
    m_messageList << "H.P : 010 - 8858 - 5828";
    m_messageList << "E-Mail : kmc5828@naver.com";
}

void StartWidget::setAnimMessage()
{
    if(m_messageList.isEmpty())
        return;

    QString curMessage = ui->label_animation->text();
    int index = m_messageList.indexOf(curMessage);
    if(index == m_messageList.size() -1 )
        index = 0;
    else
        index++;

    QString nextMessage = m_messageList.at(index);

    ui->label_animation->setAttribute(Qt::WA_DontShowOnScreen, true);
    ui->label_animation->setText(nextMessage);
    ui->label_animation->setAttribute(Qt::WA_DontShowOnScreen, false);
}
