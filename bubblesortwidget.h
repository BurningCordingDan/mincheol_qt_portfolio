#ifndef BUBBLESORTWIDGET_H
#define BUBBLESORTWIDGET_H

#include "sortwidget.h"

class BubbleSortWidget : public SortWidget
{
    Q_OBJECT

public:
    explicit BubbleSortWidget(QWidget *parent = 0);
    ~BubbleSortWidget();

protected:
    void sortFunction(int pointLimit);
};

#endif // BUBBLESORTWIDGET_H
