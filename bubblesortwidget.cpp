#include "bubblesortwidget.h"

#include <QPoint>

BubbleSortWidget::BubbleSortWidget(QWidget *parent)
{

}

BubbleSortWidget::~BubbleSortWidget()
{

}

void BubbleSortWidget::sortFunction(int pointLimit)
{
    int limit = pointLimit - m_currentIndex  -1;
    for(int i = 0; i < limit ; i ++)
    {
        QPoint cur = m_sortedPointList.at(i);
        QPoint next = m_sortedPointList.at(i + 1);

        if(cur.y() > next.y())
        {
            cur.setX((i+1) * 2);
            next.setX(i * 2);

            m_sortedPointList.replace(i ,  next );
            m_sortedPointList.replace(i +1, cur);
        }
    }
}
