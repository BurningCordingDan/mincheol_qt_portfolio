#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

class QStackedLayout;
namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

private:
    Ui::MainWidget *ui;
    QStackedLayout *m_mainLayout;
};

#endif // MAINWIDGET_H
