#ifndef QUADTREE_H
#define QUADTREE_H

#include <QListIterator>
#include <QPainter>
#include <QRect>

class QuadTreeNode
{
public:
    explicit QuadTreeNode();
    virtual ~QuadTreeNode();

    bool addPoint(const QPoint &point, const int &divideCount);
    void setArea(const QRect &area);

    void paintArea(QPainter &painter);
private:
    QRect area();

    bool isContains(const QPoint &pt);

    void divideList(const int &limit);
    void makeQuadNode();

    void addPoint(const QPoint &point);

private:
    QRect              m_area;
    QList<QPoint>      m_leafs;

    QuadTreeNode   *m_upperLeft;
    QuadTreeNode   *m_upperRight;
    QuadTreeNode   *m_lowerLeft;
    QuadTreeNode   *m_lowerRight;

};

class QuadTree
{

public:
    explicit QuadTree(const QRect &area);
    virtual ~QuadTree();

    void addPoint(const QPoint &point);
    void setDivideCount(const int &divideCount);

    void paintArea(QPainter &painter);
private:
    QuadTreeNode *m_header;

    int m_divideCount;


};


#endif // QUADTREE_H
