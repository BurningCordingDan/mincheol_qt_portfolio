#include "quadtree.h"
#include "quadtreewidget.h"
#include "ui_quadtreewidget.h"

#define _TIMER_INTERVAL 200
#define _POINT_LIMIT    400

//!
//!     QuadTree 이미지를 보여주는 WIdget입니다.
//!     resizeEvent마다 현 Widget의 Rect를 이용 QuadTree를 생성하고
//!     해당 영역에 속하는 Point를 _TIMER_INTERVAL 간격으로 생성합니다.
//!
//!     Point가 _POINT_LIMIT개수에 달하면 초기화 됩니다.
//!

QuadTreeWidget::QuadTreeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QuadTreeWidget),
    m_timerID(0),
    m_quadTree(NULL)
{
    ui->setupUi(this);

    // [1] timerEvent 시작 및 ID 저장.
    m_timerID = startTimer(_TIMER_INTERVAL);
}

QuadTreeWidget::~QuadTreeWidget()
{
    // [1] 저장된 ID에 따라 timerEvent 종료.
    killTimer(m_timerID);

    // [2] QuadTree 삭제.
    if(m_quadTree)
        delete m_quadTree;

    delete ui;
}

void QuadTreeWidget::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event)

    if(!m_quadTree || m_drawingArea.isEmpty())
        return;

    // [1] point 개수가 Limit 개수까지 도달하면 초기화.
    if(m_pointList.size() == _POINT_LIMIT)
    {
        m_pointList.clear();

        if(m_quadTree)
            delete m_quadTree;

        m_quadTree = new QuadTree(m_drawingArea);
    }


    // [2] 영역 내부의 무작위 Point 추출 및 추가.
    int x = rand() % m_drawingArea.width();
    int y = rand() % m_drawingArea.height();

    QPoint newPoint(x,y);
    if(!m_drawingArea.contains(newPoint))
        return;

    m_pointList.append(newPoint);
    m_quadTree->addPoint(newPoint);

    repaint();
}

void QuadTreeWidget::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)

    // [1] 현재의 Rect를 구하여 정사각형으로 변환 및 세팅.
    QRect currentRect = rect();
    makeSquareRect(currentRect);

    m_drawingArea = currentRect;

    // [2] QuadTree 및 PointList 초기화.
    m_pointList.clear();
    if(m_quadTree)
        delete m_quadTree;

    m_quadTree = new QuadTree(m_drawingArea);
}

void QuadTreeWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    if(m_drawingArea.isEmpty())
        return;

    if(!m_quadTree)
        return;

    QPainter painter(this);

    // [1] 배경.
    painter.setPen(Qt::black);
    painter.setBrush(Qt::black);
    painter.drawRect(rect());

    // [2] QuadTree가 화면 중심으로 오도록 Matrix 세팅.
    QMatrix matrix;
    calcMatrix(matrix);
    painter.setMatrix(matrix);

    // [3] 점 그리기.
    QPen redPen;
    redPen.setColor(Qt::red);
    redPen.setWidth(3);

    painter.setPen(redPen);
    painter.setBrush(Qt::red);

    foreach (const QPoint &point, m_pointList) {
        painter.drawPoint(point);
    }

    // [4] QuadTree 그리기.
    QPen yellowPen;
    yellowPen.setColor(Qt::yellow);
    yellowPen.setWidth(0);

    painter.setPen(yellowPen);
    painter.setBrush(Qt::NoBrush);
    m_quadTree->paintArea(painter);

    painter.end();
}

void QuadTreeWidget::calcMatrix(QMatrix &matrix)
{

    double centerX = rect().center().x();
    double centerY = rect().center().y();

    // [1] 화면 중심을 0,0으로 옮기기.
    QMatrix moveZeroMatrix;
    moveZeroMatrix.translate(-centerX, -centerY);

    // [2] scale 적용.
    double scale = 0.9;
    QMatrix scaleMatrix;
    scaleMatrix.scale(scale, scale);

    // [3] [1] 과정 원상복구.
    QMatrix moveCenterMatrix;
    moveCenterMatrix.translate(centerX, centerX);


    // [4] QuadTree가 화면 중심으로 오도록 추가 이동.
    double curWidth = rect().width();
    double curHeight = rect().height();

    double areaWidth = m_drawingArea.width();
    double areaHeight = m_drawingArea.height();

    double moveXGap = (curWidth - areaWidth) / 2.0;
    double moveYGap = (curHeight - areaHeight) / 2.0;

    QMatrix moveMatrix;
    moveXGap *= scale;
    moveYGap *= scale;
    moveMatrix.translate(moveXGap, moveYGap);

    // [5] Matrix 적용.
    matrix *= moveZeroMatrix;
    matrix *= scaleMatrix;
    matrix *= moveCenterMatrix;
    matrix *= moveMatrix;
}

void QuadTreeWidget::makeSquareRect(QRect &rect)
{
    int width = rect.width();
    int height = rect.height();
    if(width == height)
        return;

    // [1] 작은 크기로 맞춤.
    if(height < width)  rect.setWidth(height);
    else                rect.setHeight(width);
}
