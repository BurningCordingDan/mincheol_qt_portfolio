#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QStackedLayout;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    enum PHASE {
        _PHASE_START,
        _PHASE_MAIN
    };

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void readyToMain();

private:
    Ui::MainWindow *ui;
    QStackedLayout *m_mainLayout;
};

#endif // MAINWINDOW_H
