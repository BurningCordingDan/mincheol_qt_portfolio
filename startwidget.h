#ifndef STARTWIDGET_H
#define STARTWIDGET_H

#include <QWidget>

class QPropertyAnimation;
namespace Ui {
class StartWidget;
}

class StartWidget : public QWidget
{
    Q_OBJECT

    enum SEQUENCE_ANIM{
        _SEQUENCE_READY,
        _SEQUENCE_IN,
        _SEQUENCE_STAY,
        _SEQUENCE_OUT
    };

public:
    explicit StartWidget(QWidget *parent = 0);
    ~StartWidget();

signals:
    void buttonClicked();

private slots:
    void animation();

    // QWidget interface
protected:
    void showEvent(QShowEvent *event);
    void hideEvent(QHideEvent *event);

    void startAnimation();
    void endAnimation();

    void makeAnimMessage();
    void setAnimMessage();

private:
    Ui::StartWidget *ui;

    QPropertyAnimation *m_animation;
    SEQUENCE_ANIM       m_animSquence;

    QStringList         m_messageList;
};

#endif // STARTWIDGET_H
