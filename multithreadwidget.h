#ifndef MULTITHREADWIDGET_H
#define MULTITHREADWIDGET_H

#include <QWidget>

class QWaitCondition;
class QMutex;

namespace Ui {
class MultiThreadWidget;
}

class MultiThreadWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MultiThreadWidget(QWidget *parent = 0);
    ~MultiThreadWidget();

protected slots :
    void updateProgress();
    void updateMakerLabel(bool flag);
    void updateConsumerLabel(bool flag);

signals:
    void finishRequest();

private:
    Ui::MultiThreadWidget *ui;

    volatile int m_value;
    QWaitCondition  *m_waitCondition;
    QMutex          *m_mutex;

    QPixmap         m_redIcon;
    QPixmap         m_greenIcon;
};

#endif // MULTITHREADWIDGET_H
