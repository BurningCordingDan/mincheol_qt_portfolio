#ifndef QUADTREEWIDGET_H
#define QUADTREEWIDGET_H

#include <QWidget>

class QuadTree;
namespace Ui {
class QuadTreeWidget;
}

class QuadTreeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit QuadTreeWidget(QWidget *parent = 0);
    ~QuadTreeWidget();

protected:
    void timerEvent(QTimerEvent *event);
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

    void calcMatrix(QMatrix &matrix);
    void makeSquareRect(QRect &rect);

private:
    Ui::QuadTreeWidget *ui;

    int m_timerID;
    QRect m_drawingArea;

    QuadTree *m_quadTree;
    QList<QPoint> m_pointList;

};

#endif // QUADTREEWIDGET_H
