#-------------------------------------------------
#
# Project created by QtCreator 2017-02-17T20:34:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KimMinCheol_Code_Review
TEMPLATE = app
CONFIG += c++11
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    startwidget.cpp \
    mainwidget.cpp \
    multithreadwidget.cpp \
    quadtreewidget.cpp \
    worker.cpp \
    quadtree.cpp \
    sortwidget.cpp \
    bubblesortwidget.cpp

HEADERS  += mainwindow.h \
    startwidget.h \
    mainwidget.h \
    multithreadwidget.h \
    quadtreewidget.h \
    worker.h \
    quadtree.h \
    sortwidget.h \
    bubblesortwidget.h

FORMS    += mainwindow.ui \
    startwidget.ui \
    mainwidget.ui \
    multithreadwidget.ui \
    quadtreewidget.ui \
    sortwidget.ui

RESOURCES += \
    resource.qrc
