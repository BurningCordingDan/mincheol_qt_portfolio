#include "multithreadwidget.h"
#include "ui_multithreadwidget.h"
#include "worker.h"

#include <QMutex>
#include <QThread>
#include <QWaitCondition>

//!
//!     MutiThread 클릭시 화면에 세팅되는 Widget입니다.
//!     공유자원 및 공유자원에 대한 Mutex와 WaitCondition을 지니고 있으며
//!     Worker 클래스를 이용  maker / consumer 객체를 생성 Thread로 동작시키는 역활을 합니다.
//!

MultiThreadWidget::MultiThreadWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MultiThreadWidget),
    m_value(_VALUE_MAX_COUNT),
    m_waitCondition(new QWaitCondition),
    m_mutex(new QMutex),
    m_redIcon(QPixmap(":/Icon/resource/icon/Red.png")),
    m_greenIcon(QPixmap(":/Icon/resource/icon/Green.png"))
{
    ui->setupUi(this);

    // [1] progress 초기화.
    ui->progressBar->setMinimum(-1);
    ui->progressBar->setMaximum(_VALUE_MAX_COUNT + 1);

    // [2] maker 생성 및 공유자원, mutex, waitcondition 전달.
    Worker *maker = new Worker;
    maker->setValue(&m_value);
    maker->setMutex(m_mutex);
    maker->setWaitCondition(m_waitCondition);
    connect(maker, SIGNAL(updateRequest()), this, SLOT(updateProgress()));
    connect(maker, SIGNAL(workFlagChanged(bool)), this, SLOT(updateMakerLabel(bool)));

    // [3] consumer 생성 및 공유자원, mutex, waitcondition 전달.
    Worker *consumer = new Worker;
    consumer->setValue(&m_value);
    consumer->setMutex(m_mutex);
    consumer->setWaitCondition(m_waitCondition);
    connect(consumer, SIGNAL(updateRequest()), this, SLOT(updateProgress()));
    connect(consumer, SIGNAL(workFlagChanged(bool)), this, SLOT(updateConsumerLabel(bool)));

    // [4] 각 Thread 생성 및 이동, thread start시 정해진 함수 동작되도록 설정.
    QThread *makerThread = new QThread;
    maker->moveToThread(makerThread);
    connect(makerThread, SIGNAL(started()), maker, SLOT(make()));
    connect(makerThread, SIGNAL(finished()), maker, SLOT(deleteLater()), Qt::QueuedConnection);
    connect(makerThread, SIGNAL(finished()), makerThread, SLOT(deleteLater()), Qt::QueuedConnection);

    connect(this, SIGNAL(finishRequest()), makerThread, SLOT(quit()), Qt::DirectConnection);

    QThread *consumerThread = new QThread;
    consumer->moveToThread(consumerThread);
    connect(consumerThread, SIGNAL(started()), consumer, SLOT(consume()));    
    connect(consumerThread, SIGNAL(finished()), consumer, SLOT(deleteLater()), Qt::QueuedConnection);
    connect(consumerThread, SIGNAL(finished()), consumerThread, SLOT(deleteLater()), Qt::QueuedConnection);

    connect(this, SIGNAL(finishRequest()), consumerThread, SLOT(quit()), Qt::DirectConnection);


    // [5] thread 시작.
    makerThread->start();
    consumerThread->start();

    updateMakerLabel(true);
    updateConsumerLabel(true);
}

MultiThreadWidget::~MultiThreadWidget()
{
    QMutexLocker locker(m_mutex);
    m_value = -1;

    emit finishRequest();

    delete ui;
}

void MultiThreadWidget::updateProgress()
{
    QMutexLocker locker(m_mutex);
    ui->progressBar->setValue(m_value);
}

void MultiThreadWidget::updateMakerLabel(bool flag)
{
    if(flag)
    {
        ui->label_Maker->setText("Maker : Working");
        ui->label_Maker_Icon->setPixmap(m_greenIcon);
    }
    else
    {
        ui->label_Maker->setText("Maker : Waiting");
        ui->label_Maker_Icon->setPixmap(m_redIcon);
    }

}

void MultiThreadWidget::updateConsumerLabel(bool flag)
{
    if(flag)
    {
        ui->label_Consumer->setText("Consumer : Working");
        ui->label_Consumer_Icon->setPixmap(m_greenIcon);
    }
    else
    {
        ui->label_Consumer->setText("Consumer : Waiting");
        ui->label_Consumer_Icon->setPixmap(m_redIcon);
    }
}
