#include "mainwidget.h"
#include "mainwindow.h"
#include "startwidget.h"
#include "ui_mainwindow.h"

#include <QStackedLayout>

//!
//!     기본이 되는 MainWindow입니다.
//!     central widget에 stackedLayout을 장착하여 Start Widget이 종료된 이후
//!     MainWidget이 화면에 설정되도록 구현 되었습니다.
//!

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_mainLayout(new QStackedLayout)
{
    setAttribute(Qt::WA_DontShowOnScreen, true);

    ui->setupUi(this);
    ui->mainToolBar->hide();

    // [1] central widget 생성.
    QWidget *central = new QWidget();
    central->setAttribute(Qt::WA_DontShowOnScreen, true);
    central->setLayout(m_mainLayout);

    // [2] start widget 생성.
    StartWidget* startWidget = new StartWidget;
    m_mainLayout->addWidget(startWidget);
    connect(startWidget, SIGNAL(buttonClicked()), this, SLOT(readyToMain()), Qt::QueuedConnection);

    // [3] main widget 생성.
    MainWidget* mainWidget = new MainWidget;
    m_mainLayout->addWidget(mainWidget);

    // [4] central widget 장착.
    setCentralWidget(central);
    central->setAttribute(Qt::WA_DontShowOnScreen, false);

    // [5] 초기값 start widget으로 세팅.
    m_mainLayout->setCurrentIndex(_PHASE_START);

    setAttribute(Qt::WA_DontShowOnScreen, false);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_mainLayout;
}

void MainWindow::readyToMain()
{
    m_mainLayout->setCurrentIndex(_PHASE_MAIN);
}
