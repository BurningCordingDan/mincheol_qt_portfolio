#include "mainwindow.h"
#include <QApplication>

//!
//!     제작일자 : 2017-02-18.
//!     제작자 김민철.
//!
//!
//!     설명 : 코드 리뷰용 프로젝트 입니다.
//!            Qt 5.6 + Msvc 2015 32Bit 환경에서 제작 되었습니다.
//!
//!            기본적인 Qt의 UI 사용법, Layout, animation, Connect연결 과
//!            Thread 사용, 간이 QuadTree 알고리즘등이 구현 되어 있습니다.
//!
//!            MutiThread는 Maker와 Consumer가 공유자원을 생성 및 소비 하는 과정을 Progress로 표현하였습니다.
//!            QuadTree는 지정된 영역에 Point를 생성하여 QuadTree가 어떻게 분할 되는지를 이미지로 표현 하였습니다.
//!

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
