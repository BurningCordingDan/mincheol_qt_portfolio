#include "sortwidget.h"
#include "mainwidget.h"
#include "multithreadwidget.h"
#include "quadtreewidget.h"
#include "ui_mainwidget.h"
#include "bubblesortwidget.h"

#include <QStackedLayout>

//!
//!     Ready Widget에서 Start를 클릭하면 화면에 나타나느 Widget입니다.
//!     ListWidget 값이 변경됨에 따라 세팅이된 MutiThreadWidget / QuadTreeWidget이 화면에 변경되도록 설정 되었습니다.
//!

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget),
    m_mainLayout(new QStackedLayout)
{
    setAttribute(Qt::WA_DontShowOnScreen, true);
    ui->setupUi(this);

    // [1] multi thread widget 생성.
    MultiThreadWidget * multiThread = new MultiThreadWidget;
    multiThread->setAttribute(Qt::WA_DontShowOnScreen, true);
    m_mainLayout->addWidget(multiThread);
    multiThread->setAttribute(Qt::WA_DontShowOnScreen, false);

    // [2] quadtree widget 생성.
    QuadTreeWidget *quadTree = new QuadTreeWidget;
    quadTree->setAttribute(Qt::WA_DontShowOnScreen, true);
    m_mainLayout->addWidget(quadTree);
    quadTree->setAttribute(Qt::WA_DontShowOnScreen, false);


    // [2] bubble widget 생성.
    BubbleSortWidget *bubbleSort = new BubbleSortWidget;
    bubbleSort->setAttribute(Qt::WA_DontShowOnScreen, true);
    m_mainLayout->addWidget(bubbleSort);
    bubbleSort->setAttribute(Qt::WA_DontShowOnScreen, false);

    ui->mainWidget->setLayout(m_mainLayout);

    setAttribute(Qt::WA_DontShowOnScreen, false);

    // [3] listwidget의 active에 따라 layout 변경되도록 연결.
    connect(ui->listWidget, &QListWidget::currentRowChanged, [=](int curRow){
        m_mainLayout->setCurrentIndex(curRow);
    });

    // [4] listWidget item Height 확장.
    QSize itemSize = ui->listWidget->item(0)->sizeHint();
    itemSize.setHeight(50);

    ui->listWidget->item(0)->setSizeHint(itemSize);
    ui->listWidget->item(1)->setSizeHint(itemSize);
}

MainWidget::~MainWidget()
{
    delete ui;
}
