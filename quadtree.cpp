#include "quadtree.h"

//!     QuadTree를 구성하는 Node 클래스 입니다.
//!     Node는 Point의 개수가 한계에 달했을 때에 4분할이 되도록 구현 되어 있습니다.
//!

QuadTreeNode::QuadTreeNode() :
    m_area(QRect()),
    m_upperLeft (NULL),
    m_upperRight(NULL),
    m_lowerLeft (NULL),
    m_lowerRight(NULL)
{
}

QuadTreeNode::~QuadTreeNode()
{
    if(!m_upperLeft )        delete m_upperLeft;
    if(!m_upperRight)        delete m_upperRight;
    if(!m_lowerLeft )        delete m_lowerLeft;
    if(!m_lowerRight)        delete m_lowerRight;

    m_leafs.clear();
}

bool QuadTreeNode::addPoint(const QPoint &point, const int &divideCount)
{
    // [1] 영역 확인.
    if(!isContains(point))
        return false;

    // [2] 자식에 포함되는지 확인.
    if(m_upperLeft  && m_upperLeft->addPoint(point, divideCount))    return true;
    if(m_upperRight && m_upperRight->addPoint(point, divideCount))  return true;
    if(m_lowerLeft  && m_lowerLeft->addPoint(point, divideCount))    return true;
    if(m_lowerRight && m_lowerRight->addPoint(point, divideCount))  return true;

    // [3] 자신에게 추가 및 분할 요청.
    addPoint(point);
    divideList(divideCount);

    return true;
}

QRect QuadTreeNode::area()
{
    return m_area;
}

void QuadTreeNode::setArea(const QRect &area)
{
    m_area = area;
}

void QuadTreeNode::paintArea(QPainter &painter)
{
    if(m_upperLeft )    m_upperLeft->paintArea(painter);
    if(m_upperRight)    m_upperRight->paintArea(painter);
    if(m_lowerLeft )    m_lowerLeft->paintArea(painter);
    if(m_lowerRight)    m_lowerRight->paintArea(painter);

    painter.drawRect(m_area);
}

bool QuadTreeNode::isContains(const QPoint &pt)
{
    return m_area.contains(pt);
}

void QuadTreeNode::divideList(const int &limit)
{
    // [1] 분할 여부 판별.
    if(m_leafs.size() < limit)
        return;

    // [2] 자식 생성.
    makeQuadNode();

    // [3] 영역별 포인트 분배.
    QList<QPoint> tempItems;
    QListIterator<QPoint> iterator(m_leafs);

    while (iterator.hasNext())
    {
        QPoint point = iterator.next();

        if(m_upperLeft->isContains(point))          m_upperLeft->addPoint(point);
        else if(m_upperRight->isContains(point))    m_upperRight->addPoint(point);
        else if(m_lowerLeft->isContains(point))     m_lowerLeft->addPoint(point);
        else if(m_lowerRight->isContains(point))    m_lowerRight->addPoint(point);
        else                                        tempItems.append(point);
    }

    m_leafs.clear();
    m_leafs.append(tempItems);

    // [4] 자식에게 분할 요청.
    m_upperLeft ->divideList(limit);
    m_upperRight->divideList(limit);
    m_lowerLeft ->divideList(limit);
    m_lowerRight->divideList(limit);
}

void QuadTreeNode::makeQuadNode()
{
    // [1] 현재 영역을 가지고 4등분 하여 자식 생성.

    if(!m_upperLeft)
    {
        m_upperLeft     = new QuadTreeNode;
        QRect area;
        area.setTop(m_area.top());
        area.setBottom(m_area.center().y());
        area.setLeft(m_area.left());
        area.setRight(m_area.center().x());

        m_upperLeft ->setArea(area);
    }

    if(!m_upperRight)
    {
        m_upperRight    = new QuadTreeNode;
        QRect area;
        area.setTop(m_area.top());
        area.setBottom(m_area.center().y());
        area.setLeft(m_area.center().x());
        area.setRight(m_area.right());

        m_upperRight->setArea(area);
    }

    if(!m_lowerLeft)
    {
        m_lowerLeft     = new QuadTreeNode;
        QRect area;
        area.setTop(m_area.center().y());
        area.setBottom(m_area.bottom());
        area.setLeft(m_area.left());
        area.setRight(m_area.center().x());

        m_lowerLeft ->setArea(area);
    }

    if(!m_lowerRight)
    {
        m_lowerRight    = new QuadTreeNode;
        QRect area;
        area.setTop(m_area.center().y());
        area.setBottom(m_area.bottom());
        area.setLeft(m_area.center().x());
        area.setRight(m_area.right());

        m_lowerRight->setArea(area);
    }
}

void QuadTreeNode::addPoint(const QPoint &point)
{
    m_leafs.append(point);
}


//!     QuadTree를 클래스 입니다.
//!     기본적으로 Main이 되는 Node와 분할개수 지니고 있습니다.
//!

QuadTree::QuadTree(const QRect &area) :
    m_header(new QuadTreeNode),
    m_divideCount(5)
{
    m_header->setArea(area);
}

QuadTree::~QuadTree()
{
    delete m_header;
}

void QuadTree::addPoint(const QPoint &point)
{
    m_header->addPoint(point, m_divideCount);
}

void QuadTree::setDivideCount(const int &divideCount)
{
    m_divideCount = divideCount;
}

void QuadTree::paintArea(QPainter &painter)
{
    m_header->paintArea(painter);
}
